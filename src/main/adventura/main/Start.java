/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package main;

import gui.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import logika.Hra;
import logika.IHra;
import logika.prikazy.PrikazJit;
import uiText.TextoveRozhrani;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/*******************************************************************************
 * Trida Start je hlavni tridou projektu jednoduche textove a graficke hry.
 *
 * @author Adam Fejtek
 * @version ZS 22/23
 */
public class Start extends Application {

    private TextArea textArea;
    private BorderPane borderPane;
    private TextField uzivatelskyVstup;
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;
    private PanelVeci panelVeci;
    private PanelPenezenky panelPenezenky;
    private HraciPlocha hraciPlocha;

    /***************************************************************************
     * Metoda, prostrednictvim niz se spousti cela aplikace
     *
     * @param args Parametry z prikazoveho radku
     */
    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("text")) {
            IHra hra = Hra.getHra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
            return;
        }
        Application.launch(args);
    }

    /**
     * Metoda zodpovedna za rizeni prubehu slozeni celeho grafickeho rozhrani.
     *
     * @param primaryStage Primarni stage aplikace.
     */
    @Override
    public void start(Stage primaryStage) {
        borderPane = new BorderPane();
        MenuBar menuBar = pripravMenu();
        VBox vBox = new VBox(menuBar, borderPane);
        Scene scene = new Scene(vBox, 1000.0, 750.0);
        primaryStage.setScene(scene);

        pripravPanely(primaryStage);
    }

    /**
     * Metoda dava dohromady vsechny panely grafickeho rozhrani od
     * hraci plochy az po panelu penezenky a jejich chovani.
     *
     * @param primaryStage Primarni stage aplikace.
     */
    public void pripravPanely(Stage primaryStage) {
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMouseTransparent(true);
        textArea.setFocusTraversable(false);
        textArea.setText(Hra.getHra().vratUvitani());
        borderPane.setCenter(textArea);

        Label labelVstupu = new Label("Zadejte prikaz: ");
        uzivatelskyVstup = new TextField();
        FlowPane flowPane = new FlowPane();
        flowPane.getChildren().addAll(labelVstupu, uzivatelskyVstup);
        flowPane.setAlignment(Pos.CENTER);
        borderPane.setBottom(flowPane);
        uzivatelskyVstup.requestFocus();

        hraciPlocha = new HraciPlocha();
        borderPane.setTop(hraciPlocha.getAnchorPane());

        panelVychodu = new PanelVychodu();
        borderPane.setRight(panelVychodu.getListView());

        BorderPane borderPaneVeci = new BorderPane();
        panelBatohu = new PanelBatohu(textArea);
        panelVeci = new PanelVeci(textArea);
        panelPenezenky = new PanelPenezenky();

        borderPaneVeci.setLeft(panelVeci.getVBox());
        borderPaneVeci.setRight(panelBatohu.getVBox());
        borderPaneVeci.setBottom(panelPenezenky.getLabel());
        borderPane.setLeft(borderPaneVeci);

        panelVychodu.getListView().setOnMouseClicked(mouseEvent -> {
            String cil = panelVychodu.getListView().getSelectionModel().getSelectedItem();
            if(cil==null) return;
            zpracujPrikaz(PrikazJit.NAZEV+" "+cil);
        });

        uzivatelskyVstup.setOnAction(event -> {
            zpracujPrikaz(uzivatelskyVstup.getText());
            uzivatelskyVstup.clear();
            if (Hra.getHra().konecHry()) {
                uzivatelskyVstup.setDisable(true);
                uzivatelskyVstup.setText("KONEC HRY");
                panelVychodu.getListView().setDisable(true);
                panelBatohu.getVBox().setDisable(true);
                panelVeci.getVBox().setDisable(true);
            }
        });

        primaryStage.show();
    }

    /**
     * Metoda postupne slozi cely menu bar. Soucasti menu baru jsou kategorie
     * soubor a napoveda. Pomoci menu baru je mozne hru restartovat, ukoncit,
     * zobrazit informace o aplikaci ci napovedu ke hre. Jednotlivym funkcim
     * nastavi hotkeys pro rychlou aktivaci.
     *
     * @return Hotovy objekt MenuBaru pripraveny pro pouziti.
     */
    private MenuBar pripravMenu() {
        MenuBar menuBar = new MenuBar();
        Menu souborMenu = new Menu("Soubor");
        Menu napovedaMenu = new Menu("Napoveda");

        ImageView novaHraIkonka = new ImageView(new Image(getClass().getResourceAsStream("/david.png"),
                20.0, 20.0, false, false));
        MenuItem novaHraMenuItem = new MenuItem("Nova Hra", novaHraIkonka);
        novaHraMenuItem.setOnAction(event -> resetHra());
        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+R"));

        MenuItem konecMenuItem = new MenuItem("Konec Hry");
        konecMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Q"));
        konecMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Opravdu chcete ukoncit hru?");
            alert.setContentText("Ukoncenim hry prijdete o veskery postup ve hre a pri pristim pruchodu hry budete zacinat uplne od zacatku.");
            alert.showAndWait();
            if(alert.getResult() == ButtonType.OK) {
                System.exit(0);
            }
        });

        MenuItem oAplikaciMenuItem = new MenuItem("O Aplikaci");
        oAplikaciMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
        MenuItem napovedaMenuItem = new MenuItem("Napoveda");
        napovedaMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();

        souborMenu.getItems().addAll(novaHraMenuItem, separatorMenuItem, konecMenuItem);
        napovedaMenu.getItems().addAll(oAplikaciMenuItem, napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("O aplikaci");
            alert.setHeaderText("Dope game - verze ZS22");
            alert.setContentText("Tento projekt je vysledkem seminarnich praci predmetu 4IT101 (kod hry) a 4IT115 (kod GUI).\n");
            alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(getClass().getResource
                    ("/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
        return menuBar;
    }

    /**
     * Metoda slouzici pro resetovani hry a grafickeho rozhrani do puvodniho stavu.
     */
    private void resetHra() {
        Hra.resetHra();

        textArea.clear();
        textArea.appendText(Hra.getHra().vratUvitani());

        uzivatelskyVstup.setDisable(false);
        uzivatelskyVstup.clear();
        uzivatelskyVstup.requestFocus();

        panelVychodu.getListView().setDisable(false);
        panelBatohu.getVBox().setDisable(false);
        panelVeci.getVBox().setDisable(false);

        hraciPlocha.update();
        panelVychodu.update();
        panelBatohu.update();
        panelVeci.update();
        panelPenezenky.update();
    }

    /**
     * Metoda slouzici jako prettifier vystupu funkce zpracujPrikaz() tridy Hra.
     *
     * @param prikaz Prikaz zadany uzivatelem
     */
    private void zpracujPrikaz(String prikaz) {
        String vypis = Hra.getHra().zpracujPrikaz(prikaz);
        textArea.appendText("\n" + vypis + "\n");
    }

}
