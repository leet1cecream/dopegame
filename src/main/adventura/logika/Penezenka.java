package logika;

import observer.IObserver;
import observer.ISubject;

import java.util.HashSet;
import java.util.Set;

/**
 * Trida Penezenka - trida urcena pro uchovavani informaci o tom
 * kolik ma hrac u sebe korun. Nelze s ni jit do minusu.
 *
 * @author Adam Fejtek
 * @version ZS 22/23
 */
public class Penezenka implements ISubject {
    private int penize;
    private static final Set<IObserver> observerSet = new HashSet<>();

    public Penezenka() {
        penize = 5;
    }

    /**
     * Prida pozadovanou sumu penez do penezenky
     *
     * @param castka Castka, kterou chceme pridat
     */
    public void pridatPenize(int castka) {
        penize += castka;
        notifyObservers();
    }

    /**
     * Odebere pozadovanou castku z penezenky
     *
     * @param castka Castka, kterou chceme odebrat
     * @return Vrati true, pokud se podarilo castku odebrat, jinak false
     */
    public boolean odebratPenize(int castka) {
        if (castka > penize) return false;
        penize -= castka;
        notifyObservers();
        return true;
    }

    /**
     * Vrati pocet penez v penezence
     *
     * @return Pocet penez v penezence
     */
    public int getPenize() {
        return penize;
    }

    /**
     * Slouzi pro vygenerovani textu obsahu penezenky v raci zobrazeni uzivateli
     *
     * @return Text obsahu penezenky
     */
    public String zobrazObsah() {
        StringBuilder obsahText = new StringBuilder("Obsah penezenky: ");
        if (penize > 0) {
            obsahText.append(penize).append(" korun");
        }
        else {
            obsahText.append("nic");
        }
        return obsahText.toString();
    }

    @Override
    public void addObserver(IObserver observer) {
        observerSet.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observerSet.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(IObserver observer : observerSet) {
            observer.update();
        }
    }

}
