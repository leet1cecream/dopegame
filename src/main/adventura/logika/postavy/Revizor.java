package logika.postavy;

import logika.Vec;
import logika.Postava;

public class Revizor extends Postava {

    public Revizor(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

}
