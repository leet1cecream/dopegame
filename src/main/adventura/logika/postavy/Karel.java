package logika.postavy;

import logika.Vec;
import logika.Postava;

/**
 * Class Karel - Obsahuje vlastnosti a chovani Karla. Dedi z classy Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Karel extends Postava {

    public Karel(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

}
