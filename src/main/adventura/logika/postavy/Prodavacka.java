package logika.postavy;

import logika.Hra;
import logika.Postava;
import logika.Vec;

/**
 * Class Prodavacka - Obsahuje chovani prodavacky. Rozsiruje classu Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Prodavacka extends Postava {

    public Prodavacka(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public Vec vymenitVec(Vec vec) {
        Hra.getHra().getHerniPlan().getPenezenka().pridatPenize(vec.getCena());
        return super.vymenitVec(vec);
    }

}
