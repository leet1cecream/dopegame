package logika.postavy;

import logika.Hra;
import logika.Postava;
import logika.Vec;

/**
 * Class Krysa - Obsahuje vlastnosti a chovani Karla. Dedi z classy Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Krysa extends Postava {

    public Krysa(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public String mluvit() {
        Hra.getHra().setKonecHry(true);
        return "Pri mluveni s krysou na tebe skocila a vykousala ti oci. Umrel jsi.";
    }

}
