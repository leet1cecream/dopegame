package logika.postavy;

import logika.Vec;
import logika.Hra;
import logika.Postava;

/**
 * Class Sekuritak - Obsahuje vlastnosti a chovani Karla. Dedi z classy Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Sekuritak extends Postava {
    public Sekuritak(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public String mluvit() {
        Hra.getHra().setKonecHry(true);
        return "Sekuritakovy ses nelibil a tak te vyhodil ven. Spadl si na hlavu a umrel jsi.";
    }

}
