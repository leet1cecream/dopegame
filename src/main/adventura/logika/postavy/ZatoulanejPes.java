package logika.postavy;

import logika.Vec;
import logika.Hra;
import logika.Postava;

/**
 * Class ZatoulanejPes - Obsahuje vlastnosti a chovani Karla. Dedi z classy Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class ZatoulanejPes extends Postava {

    public ZatoulanejPes(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public Vec vymenitVec(Vec vec) {
        Hra.getHra().setKonecHry(true);
        return super.vymenitVec(vec);
    }

}
