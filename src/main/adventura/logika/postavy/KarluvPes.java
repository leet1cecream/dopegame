package logika.postavy;

import logika.Vec;
import logika.Hra;
import logika.Postava;

/**
 *
 */
public class KarluvPes extends Postava {

    public KarluvPes(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public String mluvit() {
        Hra.getHra().setKonecHry(true);
        return "Karluv pes se utrhnul z voditka a vojel ti nohu. Umrel jsi.";
    }

    @Override
    public Vec vymenitVec(Vec vec) {
        Hra.getHra().getHerniPlan().getAktualniProstor().vlozVec(new Vec("placatka", "placatka", true, 0));
        return null;
    }

}
