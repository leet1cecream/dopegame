package logika.postavy;

import logika.Postava;
import logika.HerniPlan;
import logika.Hra;
import logika.Vec;

/**
 * Class Fanousek - Obsahuje vlastnosti a chovani Karla. Dedi z classy Postava.
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Fanousek extends Postava {

    public Fanousek(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public String mluvit() {
        HerniPlan herniPlan = Hra.getHra().getHerniPlan();
        herniPlan.getBatoh().vlozVec(new Vec("cigo", "Cigo", true, 5));
        herniPlan.getPenezenka().pridatPenize(100);
        return super.mluvit();
    }

}
