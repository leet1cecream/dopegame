package logika.postavy;

import logika.Vec;
import logika.Hra;
import logika.Postava;

public class Pochybnak extends Postava {
    public Pochybnak(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        super(jmeno, screenJmeno, mluvPred, mluvPo, mluvChce, mluvNechce, vecChce, vecMa, penize);
    }

    @Override
    public String mluvit() {
        String odpoved = """
                Pochybnak: Cau kamo, co potrebujes?
                Ty: Cau cau. Nemas hele prosimte nejaky matro?
                Pochybnak: 700 za gram nebo 130 korun za to co mam v kapse
                """;
        if (Hra.getHra().getHerniPlan().getPenezenka().getPenize() >= 130) {
            odpoved += """
                    Ty: Konecne nekdo schopnej visco. Tady mas.
                    Pochybnak: Tady mas G a trochu k tomu.
                    Ty: Tyjo dik moc kamo ty ses real.""";
            Hra.getHra().getHerniPlan().getBatoh().vlozVec(new Vec("parno", "Parno", false, 0));
        }
        else {
            odpoved += "Ty: Sorry kamo tak to fakt nemam, ale urcite se vratim.";
        }
        return odpoved;
    }

}
