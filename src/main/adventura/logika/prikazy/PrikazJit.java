package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;
import logika.Prostor;

/**
 * Třída PrikazJit implementuje pro hru příkaz jit.
 * Tato třída je součástí jednoduché textové hry.
 * @author Jarmila Pavlickova, Luboš Pavlíček
 * @version pro školní rok 2016/2017
 */
public class PrikazJit implements IPrikaz {

    public static final String NAZEV = "jit";
    private final HerniPlan plan;

    public PrikazJit(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Vejde do prostoru po zadani prikazu "jit [prostor]".
     * Pokud dany prostor neexistuje, vypise se chybova hlaska.
     * Obsahuje specialni chovani jako napriklad omezeni vstupu
     * pro ruzne prostory.
     * @param parametry Jako parametr obsahuje nazev prostoru, do ktereho se ma jit
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "A kam asi jako chces abych sel kamo?";
        }

        Prostor aktualniProstor = plan.getAktualniProstor();
        Prostor sousedniProstor = aktualniProstor.vratSousedniProstor(parametry[0]);

        if (sousedniProstor == null) {
            return "A jak se tam asi jako dostanu?";
        }

        if (sousedniProstor.getNazev().equals("hlavak")
            && aktualniProstor.getNazev().equals("park")
            && !plan.getBatoh().obsahujeVec("dope_kalhoty")) {
                return sousedniProstor.getNemuzeVstupit();
        }
        else if (sousedniProstor.getNazev().equals("metro")
                 && aktualniProstor.getNazev().equals("hlavak")
                 && !aktualniProstor.getPostava("revizor").getVymena()) {
            aktualniProstor.getPostava("revizor").mluvit();
            return sousedniProstor.getNemuzeVstupit();
        }

        plan.setAktualniProstor(sousedniProstor);
        return sousedniProstor.dlouhyPopis();
    }
    
    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
