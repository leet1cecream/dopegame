package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;

/**
 * Trida PrikazMluvit - Prikaz pro mluveni s postavou.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazMluvit implements IPrikaz {

    public static final String NAZEV = "mluvit";
    private final HerniPlan plan;

    public PrikazMluvit(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Promluvi s postavou po zadani prikazu "mluvit".
     * Pokud postava neexistuje, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje jmeno postavy, s kterou se ma mluvit
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Na koho mam jako mluvit kamo?";
        }
        if (!plan.getAktualniProstor().obsahujePostavu(parametry[0])) {
            return "Tenhle typek asi neni dost real no.";
        }

        return plan.getAktualniProstor().getPostava(parametry[0]).mluvit();
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
