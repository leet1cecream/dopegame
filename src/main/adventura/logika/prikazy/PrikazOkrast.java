package logika.prikazy;

import logika.HerniPlan;
import logika.Hra;
import logika.IPrikaz;
import logika.Postava;

import java.util.Random;

/**
 * Trida PrikazOkrast - Prikaz pro okradeni postavy o penize.
 * Prikaz ma nastavitelnou sanci na uspech.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazOkrast implements IPrikaz {

    public static final String NAZEV = "okrast";
    public static final int SANCE = 75;
    private final Hra hra;
    private final HerniPlan plan;
    private final Random rand;

    public PrikazOkrast(Hra hra) {
        this.hra = hra;
        plan = hra.getHerniPlan();
        rand = new Random();
    }

    /**
     * Na zaklade sance okrade postavu o penize nebo hrace zabije po zadani prikazu "okrast [postava]".
     * Pokud u sebe postava nema penize nebo neexistuje, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje nazev postavy, ktera ma byt okradena
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Jakoby netusim koho mam okrast visco kamo...";
        }
        if (!plan.getAktualniProstor().obsahujePostavu(parametry[0])) {
            return "Tenhle typek asi neni dost real no.";
        }

        Postava postava = plan.getAktualniProstor().getPostava(parametry[0]);

        if (rand.nextInt(100) >= SANCE) {
            hra.setKonecHry(true);
            return "Byl jsi pristizen pri cinu, dostal si tezkej loket a umrel jsi.";
        }

        int penize = postava.getPenize();
        if (penize == 0) {
            return postava.getScreenJmeno() + " u sebe nema zadne penize.";
        }

        plan.getPenezenka().pridatPenize(penize);
        postava.odebratPenize(penize);
        return postava.getScreenJmeno() + " byl uspesne okraden o " + penize + " korun.";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
