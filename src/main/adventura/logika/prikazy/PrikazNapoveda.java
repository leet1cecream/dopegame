package logika.prikazy;

import logika.IPrikaz;
import logika.SeznamPrikazu;

/**
 * Třída PrikazNapoveda implementuje pro hru příkaz napoveda.
 * Tato třída je součástí jednoduché textové hry.
 * @author Jarmila Pavlickova, Luboš Pavlíček
 * @version pro školní rok 2016/2017
 */
public class PrikazNapoveda implements IPrikaz {
    
    public static final String NAZEV = "napoveda";
    private final SeznamPrikazu platnePrikazy;

    public PrikazNapoveda(SeznamPrikazu platnePrikazy) {
        this.platnePrikazy = platnePrikazy;
    }

    /**
     * Vraci zakladni napovedu po zadani prikazu "napoveda".
     * @param parametry Nevyzaduje zadne vstupni parametry
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return "Cilem hry je sjet se jak pes a zustat real.\n" +
                "Tento ukol vsak nebude jednoduchy, protoze se s bezdakama jen tak nikdo nebavi\n" +
                "a navic mas na sobe poblity vintage versace jeans."
               + "\n"
               + "Muzes zadat tyto prikazy:\n"
               + platnePrikazy.vratNazvyPrikazu();
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
