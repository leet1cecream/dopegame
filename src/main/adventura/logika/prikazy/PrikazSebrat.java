package logika.prikazy;

import logika.*;

/**
 * Trida PrikazSebrat - Prikaz pro sebrani veci z prostoru.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazSebrat implements IPrikaz {

    public static final String NAZEV = "sebrat";
    final HerniPlan plan;

    public PrikazSebrat(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Sebere vec z aktualniho prostoru po zadani prikazu "sebrat [vec]".
     * Pokud vec v aktualnim prostoru neni, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje nazev veci, ktera se ma sebrat
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Jakoby netusim co mam sebrat visco kamo...";
        }
        if (!plan.getAktualniProstor().obsahujeVec(parametry[0])) {
            return "To asi neni real no.";
        }

        Vec vec = plan.getAktualniProstor().getVec(parametry[0]);

        if (!vec.getLzeSebrat()) {
            return "Tohle fakt neseberu kamo. Za tohle leda tak zaplatim jinak fakt nevim visco.";
        }

        Batoh batoh = Hra.getHra().getHerniPlan().getBatoh();
        if (batoh.jePlny()) {
            return "Tak tohle uz se mi fakt nikam nevejde bracho.";
        }

        plan.getBatoh().vlozVec(plan.getAktualniProstor().odeberVec(parametry[0]));
        return "Vlozil jsi do batohu: " + vec.getScreenNazev() + ".";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
