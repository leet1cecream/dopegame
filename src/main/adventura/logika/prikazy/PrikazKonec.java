package logika.prikazy;

import logika.Hra;
import logika.IPrikaz;

/**
 * Třída PrikazKonec implementuje pro hru příkaz konec.
 * Tato třída je součástí jednoduché textové hry.
 * @author Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class PrikazKonec implements IPrikaz {

    public static final String NAZEV = "konec";
    private final Hra hra;

    public PrikazKonec(Hra hra) {
        this.hra = hra;
    }

    /**
     * Hra se ukonci po zadani prikazu "konec".
     * @param parametry Nevyzaduje zadne vstupni parametry
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Koho chces jako ukoncit? Mam ukoncit tebe?";
        }

        hra.setKonecHry(true);
        return "Zadrzel jsi dech a uz ses nenadech.";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
