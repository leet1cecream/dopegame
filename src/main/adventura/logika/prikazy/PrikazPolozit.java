package logika.prikazy;

import logika.HerniPlan;
import logika.Vec;
import logika.IPrikaz;

/**
 * Trida PrikazPolozit - Prikaz pro polozeni veci do prostoru.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazPolozit implements IPrikaz {

    public static final String NAZEV = "polozit";
    final HerniPlan plan;

    public PrikazPolozit(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Polozi vec do aktualniho prostoru po zadani prikazu "polozit [vec]".
     * Pokud vec neni v batohu, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje nazev veci, ktera se ma polozit
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Jakoby netusim co mam polozit visco kamo...";
        }

        if (!plan.getBatoh().obsahujeVec(parametry[0])) {
            return "Kde bych to asi tak vzal.";
        }

        Vec vec = plan.getBatoh().vyndejVec(parametry[0]);
        plan.getAktualniProstor().vlozVec(vec);
        plan.getBatoh().notifyObservers();
        return "Polozil si na zem: " + vec.getScreenNazev() + ".";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
