package logika.prikazy;

import logika.*;

/**
 * Trida PrikazDat - Prikaz pro predani predmetu postave.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazDat implements IPrikaz {

    public static final String NAZEV = "dat";
    final HerniPlan plan;

    public PrikazDat(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Preda predmet z batohu jine postave po zadani prikazu "dat [vec] [postava]"
     * Pokud predmet nebo postava neexistuji, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje vec, kterou chce hrac dat
     *                  a postavu, ktere chce danou vec dat
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Jak mam vedet co komu predat asi.";
        }
        if (parametry.length == 1) {
            return "A komu to dat mi nereknes asi?";
        }

        Batoh batoh = plan.getBatoh();
        String nazevVeci = batoh.getNazevVeci(parametry[0]);
        
        if (!batoh.obsahujeVec(nazevVeci)) {
            return "Kde bych to asi tak vzal.";
        }

        String jmenoPostavy = parametry[1];
        Prostor aktualniProstor = plan.getAktualniProstor();
        
        if (!aktualniProstor.obsahujePostavu(jmenoPostavy)) {
            return "Tenhle typek asi neni dost real no.";
        }

        Postava postava = aktualniProstor.getPostava(jmenoPostavy);
        Vec vecChce = postava.getVecChce();

        if (vecChce == null || !postava.getMluvil() && !postava.getJmeno().equals("karluv_pes")) {
            return postava.getScreenJmeno() + " nechape co po nem chces.";
        }

        if (!vecChce.getNazev().equals(nazevVeci)) {
            return postava.getScreenJmeno() + ": " + postava.getMluvNechce();
        }

        Vec vec = postava.vymenitVec(batoh.vyndejVec(nazevVeci));

        if (vec != null) {
            batoh.vlozVec(vec);
        }

        Hra.getHra().getHerniPlan().getBatoh().notifyObservers();

        return postava.getMluvChce();
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
