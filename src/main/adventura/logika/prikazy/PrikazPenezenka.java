package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;

/**
 * Trida PrikazPenezenka - Prikaz pro zobrazeni obsahu penezenky.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazPenezenka implements IPrikaz {

    public static final String NAZEV = "penezenka";
    final HerniPlan plan;

    public PrikazPenezenka(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Vraci obsah penezenky jako vypsatelny string.
     * @param parametry Nevyzaduje zadne vstupni parametry
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return plan.getPenezenka().zobrazObsah();
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
