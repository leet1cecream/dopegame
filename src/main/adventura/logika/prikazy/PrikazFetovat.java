package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;
import logika.Hra;

/**
 * Trida PrikazFetovat - Prikaz pro uspesne ukonceni hry.
 * Pro uspesne ukonceni hry musi mit hrac s batohu parno a jehlu.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazFetovat implements IPrikaz {

    public static final String NAZEV = "fetovat";
    final Hra hra;
    private final HerniPlan plan;

    public PrikazFetovat(Hra hra) {
        this.hra = hra;
        plan = hra.getHerniPlan();
    }

    /**
     * Pokud ma v batohu hrac parno a jehlu, tak uspesne ukonci hru po zadani prikazu "fetovat".
     * V opacnem pripade se vypise chybova hlaska.
     * @param parametry Nevyzaduje zadne vstupni parametry
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (!plan.getBatoh().obsahujeVec("parno")) {
            return "Kdybych mel co tak fetuju furt kamo...";
        }

        if (!plan.getBatoh().obsahujeVec("jehla")) {
            return "Kamo kazdej vi ze snupani je nezdravy... Potrebuju jehlu!";
        }

        plan.getBatoh().vyndejVec("parno");
        plan.getBatoh().vyndejVec("jehla");
        plan.getBatoh().notifyObservers();

        hra.setKonecHry(true);
        return "Dal sis svoji posledni davku a upadl do euforie. Nic uz nejde vratit zpet. KONEC HRY";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
