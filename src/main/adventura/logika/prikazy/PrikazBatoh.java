package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;

/**
 * Trida PrikazPoloz - Prikaz pro polozeni veci do prostoru.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazBatoh implements IPrikaz {

    public static final String NAZEV = "batoh";
    final HerniPlan plan;

    public PrikazBatoh(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Vraci obsah batohu jako vypsatelny string oddeleny carkamy po zadani prikazu "batoh".
     * @param parametry Nevyzaduje zadne vstupni parametry
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return plan.getBatoh().zobrazObsah();
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
