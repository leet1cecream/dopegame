package logika.prikazy;

import logika.HerniPlan;
import logika.IPrikaz;
import logika.Vec;
import logika.Hra;

/**
 * Trida PrikazUkrast - Prikaz pro ukradeni predmetu z prostoru.
 * Slouzi pouze jako lakadlo. Pri pouziti hrace zabije.
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class PrikazUkrast implements IPrikaz {

    public static final String NAZEV = "ukrast";
    final Hra hra;
    final HerniPlan plan;

    public PrikazUkrast(Hra hra) {
        this.hra = hra;
        plan = hra.getHerniPlan();
    }

    /**
     * Zabije hrace po zadani prikazu "ukrast [vec]".
     * Pokud vec v aktualnim prostoru neni, vypise se chybova hlaska.
     * @param parametry Jako parametr obsahuje nazev veci, ktera se ma ukrast
     * @return Zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Jakoby netusim co mam ukrast visco kamo...";
        }
        if (!plan.getAktualniProstor().obsahujeVec(parametry[0])) {
            return "To asi neni real no.";
        }

        Vec vec = plan.getAktualniProstor().getVec(parametry[0]);

        if (vec.getLzeSebrat() && vec.getCena() == 0) {
            return "Proc bych tohle kradl kamo?";
        }

        hra.setKonecHry(true);
        return "Pri pokusu o loupez si uklouzl na slupce od bananu a umrel jsi.";
    }

    /**
     *  Vraci nazev prikazu (slovo ktere hrac pouziva pro jeho vyvolani)
     *  @return Nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
