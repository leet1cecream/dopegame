package logika;

import observer.IObserver;
import observer.ISubject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Trida Batoh - trida urcena pro uchovavani informaci o tom
 * jake predmety ma hrac u sebe. Nelze do nej vlozit pres limit.
 *
 * @author Adam Fejtek
 * @version ZS 22/23
 */
public class Batoh implements ISubject {

    public static final int VELIKOST = 3;
    private final Map<String, Vec> obsahBatohu;
    private static final Set<IObserver> observerSet = new HashSet<>();

    public Batoh() {
        obsahBatohu = new HashMap<>();
    }

    /**
     * Pokusi se do batohu vlozit pozadovanou vec. Pokud je batoh
     * plny, polozi se vec do aktualniho prostoru.
     *
     * @param vec Vec, ktera se ma vlozit do batohu
     * @return Vrati true, pokud se predmet podarilo vlozit, jinak false
     */
    public boolean vlozVec(Vec vec) {
        if (jePlny()) {
            Hra.getHra().getHerniPlan().getAktualniProstor().vlozVec(vec);
            notifyObservers();
            return false;
        }
        obsahBatohu.put(vec.getNazev(), vec);
        notifyObservers();
        return true;
    }

    /**
     * Vynda pozadovanou vec z batohu a vrati ji. Pokud vec
     * v batohu neni, vrati se null.
     *
     * @param nazevVeci Vec, ktera se ma vyndat
     * @return Pozadovana vec nebo null
     */
    public Vec vyndejVec(String nazevVeci) {
        return obsahBatohu.remove(getNazevVeci(nazevVeci));
    }

    /**
     * Metoda urcena pro zjisteni, zda-li batoh obsahuje urcitou vec
     *
     * @param nazevVeci Uplny nebo castecny nazev veci
     * @return Vrati true, pokud se vec v batohu nachazi, jinak false
     */
    public boolean obsahujeVec(String nazevVeci) {
        return obsahBatohu.containsKey(getNazevVeci(nazevVeci));
    }

    /**
     * Vrati jestli je batoh maximalne zaplneny
     *
     * @return True, pokud je batoh plny, jinak false
     */
    public boolean jePlny() {
        return obsahBatohu.size() >= VELIKOST;
    }

    /**
     * Metoda urcena pro hledani veci mezi vecmi v batohu
     * na zaklade uplneho nebo neuplneho nazvu veci od uzivatele.
     *
     * @param text Vstupni argument od uzivatele
     * @return Pokud se vec nasla, vrati jeji cely nazev, pokud ne, vrati se "neobsahuje"
     */
    public String getNazevVeci(String text) {
        for (String nazev : obsahBatohu.keySet()) {
            if (nazev.contains(text)) {
                return nazev;
            }
        }
        return "neexistuje";
    }

    /**
     * Slouzi pro vygenerovani textu obsahu penezenky v raci zobrazeni uzivateli
     *
     * @return Text obsahu penezenky
     */
    public String zobrazObsah() {
        StringBuilder obsahText = new StringBuilder("Obsah batohu: ");
        if (!obsahBatohu.isEmpty()) {
            for (Map.Entry<String, Vec> v : obsahBatohu.entrySet()) {
                obsahText.append(v.getValue().getNazev()).append(", ");
            }
            obsahText = new StringBuilder(obsahText.substring(0, obsahText.length() - 2));
        }
        else {
            obsahText.append("nic");
        }
        return obsahText.toString();
    }

    public Set<String> getMnozinaNazvuVeciVBatohu() {
        return obsahBatohu.keySet();
    }

    @Override
    public void addObserver(IObserver observer) {
        observerSet.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observerSet.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(IObserver observer : observerSet) {
            observer.update();
        }
    }

}
