package logika;

import logika.prikazy.*;

/**
 *  Třída Hra - třída představující logiku adventury.
 * <p>
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class Hra implements IHra {

    private static Hra SINGLETON = new Hra();
    private final SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private final HerniPlan herniPlan;
    private boolean konecHry = false;

    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    private Hra() {
        herniPlan = new HerniPlan();
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazJit(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazSebrat(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazPolozit(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazDat(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazMluvit(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazOkrast(this));
        platnePrikazy.vlozPrikaz(new PrikazUkrast(this));
        platnePrikazy.vlozPrikaz(new PrikazFetovat(this));
        platnePrikazy.vlozPrikaz(new PrikazBatoh(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazPenezenka(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
    }

    public static Hra getHra() {
        return SINGLETON;
    }

    public static Hra resetHra() {
        return SINGLETON = new Hra();
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Pise se rok 2022 a zijes uz dlouhych 5 let na ulici.\n" +
                "Tvoje jmeno je David Stilip a jedine co si kazdy den prejes je davka perniku.\n" +
                "Sehnat piko bez penez ale bohuzel neni jednoduche a proto sis uz fakt dlouho nestrelil.\n" +
                "\n" +
                "Cilem hry je sjet se jak pes a zustat real.\n" +
                "Tento ukol vsak nebude jednoduchy, protoze se s bezdakama jen tak nikdo nebavil a navic mas na sobe poblity vintage versace jeans.\n\n" +
                herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Dekuji za hrani :). KONEC HRY";
    }
    
    /** 
     * Vrací true, pokud hra skončila.
     */
    public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
    public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        System.arraycopy(slova, 1, parametry, 0, parametry.length);
        String textKVypsani;
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
        }
        else {
            textKVypsani="Fakt kamo netusim co mam delat visco...";
        }

        return textKVypsani;
    }

    /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    public void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
    /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
    public HerniPlan getHerniPlan(){
        return herniPlan;
    }

}
