package logika;

/**
 * Trida Vec - Popisuje vlastnosti a chovan veci
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Vec {

    private final String nazev;
    private final String screenNazev;
    private final boolean lzeSebrat;
    private final int cena;

    public Vec(String nazev, String screenNazev, Boolean lzeSebrat, int cena) {
        this.nazev = nazev;
        this.screenNazev = screenNazev;
        this.lzeSebrat = lzeSebrat;
        this.cena = cena;
    }

    /**
     * Vrati nazev veci
     * @return Text nazvu veci
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrati formalni nazev veci pro vypsani v textu
     * @return Formalni nazev veci
     */
    public String getScreenNazev() {
        return screenNazev;
    }

    /**
     * Vrati, zda lze predmet sebrat ci nikoliv
     * @return True pokud lze sebrat, false pokud ne
     */
    public boolean getLzeSebrat() {
        return lzeSebrat;
    }

    /**
     * Vrati cenu veci
     * @return Cena veci
     */
    public int getCena() {
        return cena;
    }

}
