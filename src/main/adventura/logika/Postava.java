package logika;

/**
 * Trida Postava - Popisuje vlastnosti a chovani postav
 *
 * @author Adam Fejtek
 * @version LS 21/22
 */
public class Postava implements IPostava {

    private final String jmeno;
    private final String screenJmeno;
    private final String mluvPred;
    private final String mluvPo;
    private final String mluvChce;
    private final String mluvNechce;
    private boolean mluvil = false;
    private boolean vymena = false;
    private Vec vecChce;
    private Vec vecMa;
    private int penize;

    public Postava(String jmeno, String screenJmeno, String mluvPred, String mluvPo, String mluvChce, String mluvNechce, Vec vecChce, Vec vecMa, int penize) {
        this.jmeno = jmeno;
        this.screenJmeno = screenJmeno;
        this.mluvPred = mluvPred;
        this.mluvPo = mluvPo;
        this.mluvChce = mluvChce;
        this.mluvNechce = mluvNechce;
        this.vecChce = vecChce;
        this.vecMa = vecMa;
        this.penize = penize;
    }

    /**
     * Urcuje, jak postava odpovy na zaklade predchozi interakce
     *
     * @return Odpoved postavy
     */
    public String mluvit() {
        String odpoved = screenJmeno + ": ";
        if (mluvil) {
            odpoved += mluvPo;
        }
        else {
            odpoved += mluvPred;
        }
        mluvil = true;
        return odpoved;
    }

    /**
     * Vrati vec, kterou postava vymenila za jeji pozadovanou
     *
     * @param vec Vec, kterou osoba obdrzi vymenou za vec co ma
     * @return Vec, kterou postava predava
     */
    public Vec vymenitVec(Vec vec) {
        vymena = true;
        Vec vecVratit = vecMa;
        vecMa = vec;
        vecChce = null;
        return vecVratit;
    }

    /**
     * Vrati jmeno postavy
     *
     * @return Text jmeno postavy
     */
    public String getJmeno() {
        return jmeno;
    }

    /**
     * Vrati formalni jmeno postavy pro vypsani v textu
     *
     * @return Text formalniho jmena postavy
     */
    public String getScreenJmeno() {
        return screenJmeno;
    }

    /**
     * Vrati odpoved pro prvotni interakci s postavou
     *
     * @return Text odpovedi
     */
    public String getMluvPred() {
        return mluvPred;
    }

    /**
     * Varti odpoved po prvotni interakci s postavou
     *
     * @return Text odpovedi
     */
    public String getMluvPo() {
        return mluvPo;
    }

    /**
     * Varti odpoved na obdrzeni pozadovaneho predmetu
     *
     * @return Text odpovedi
     */
    public String getMluvChce() {
        return mluvChce;
    }

    /**
     * Vrati odpoved na pokus o obdrzeni nechteneho predmetu
     *
     * @return Text odpovedi
     */
    public String getMluvNechce() {
        return mluvNechce;
    }

    /**
     * Vrati, zda jiz probehla prvotni interakce s postavou
     *
     * @return Vrati true, pokud jiz interakce probehla, jinak false
     */
    public boolean getMluvil() {
        return mluvil;
    }

    /**
     * Vrati, zda jiz probehla s postavou vymena pozadovane veci
     *
     * @return Vrati true, pokud jiz problehla vymena, jinak false
     */
    public boolean getVymena() {
        return vymena;
    }

    /**
     * Vrati predmet, ktery postava pozaduje
     *
     * @return Pozadovany predmet
     */
    public Vec getVecChce() {
        return vecChce;
    }

    /**
     * Vrati pocet penez, ktere ma postava u sebe
     *
     * @return Pocet penez
     */
    public int getPenize() {
        return penize;
    }

    /**
     * Odebere pozadovanou sumu penez z penezenky
     *
     * @param castka Castka, ktera se ma odebrat
     * @return Vrati true, pokud bylo odebrani castky uspesne, jinak false.
     */
    public boolean odebratPenize(int castka) {
        if (castka <= penize) {
            penize -= castka;
            return true;
        }
        return false;
    }

}
