package logika;

import logika.postavy.*;
import observer.IObserver;
import observer.ISubject;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * <p>
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan implements ISubject {

    private Prostor aktualniProstor;
    private final Batoh batoh;
    private final Penezenka penezenka;
    private static final Set<IObserver> observerSet = new HashSet<>();

    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        batoh = new Batoh();
        penezenka = new Penezenka();
        zalozHru();
    }

    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozHru() {
        // vytvářejí se jednotlivé prostory
        Prostor park = new Prostor("park", "Park u Hlavaku plnej odpadku", "", 580, 220);
        Prostor podMostem = new Prostor("pod_mostem", "Pod mostem vedle Hlavaku", "", 320, 40);
        Prostor kontejner = new Prostor("kontejner", "Smradlavej kontejner pod mostem", "", 80, 40);
        Prostor vecerka = new Prostor("vecerka", "Klasicka zluta vecerka", "", 80, 220);
        Prostor hlavak = new Prostor("hlavak", "Budova hlavniho nadrazi Praha", "Sekuritak: S tema poblitejma kalhotama sem ani nelez!", 580, 40);
        Prostor metro = new Prostor("metro", "Stanice metra Hlavni nadrazi",
                """
                        Revizor: Dobry den, jizdenku prosim.
                        Ty: Visco kamo me vypadla z kapsy jakoby. Revizor: No tak to vas budu muset poprosit, aby jste opustil prostory metra.
                        Ty: Ale notak kamo...
                        Revizor: Kdybych nebyl strizlivej, tak vas mozna i pustim.
                        """, 830, 40);
        Prostor naStrzi = new Prostor("na_strzi", "Na Strzi", "", 830, 220);

        park.setVychod(vecerka);
        park.setVychod(podMostem);
        park.setVychod(hlavak);
        vecerka.setVychod(park);
        podMostem.setVychod(kontejner);
        podMostem.setVychod(park);
        kontejner.setVychod(podMostem);
        hlavak.setVychod(metro);
        hlavak.setVychod(park);
        metro.setVychod(hlavak);
        metro.setVychod(naStrzi);
        naStrzi.setVychod(metro);

        Vec vajgl = new Vec("vajgl", "Vajgl", true, 0);
        Vec flaska = new Vec("pivni_flaska", "Pivni flaska", true, 3);
        Vec placatka = new Vec("placatka", "Placatka", true, 0);
        Vec kalhoty = new Vec("dope_kalhoty", "Dope kalhoty", true, 0);
        Vec cigarety = new Vec("cigarety", "Cigarety", false, 150);
        Vec vajnos = new Vec("vajnos", "Vajnos", false, 49);
        Vec vodka = new Vec("vodka", "Vodka", false, 200);
        Vec cigo = new Vec("cigo", "Cigo", true, 5);
        Vec jehla = new Vec("jehla", "Jehla", true, 0);
        Vec klacik = new Vec("klacik", "Klacik", true, 0);

        park.vlozVec(vajgl);
        park.vlozVec(flaska);
        kontejner.vlozVec(kalhoty);
        vecerka.vlozVec(cigarety);
        vecerka.vlozVec(vajnos);
        vecerka.vlozVec(vodka);
        naStrzi.vlozVec(jehla);
        naStrzi.vlozVec(klacik);

        Karel karel = new Karel("bezdak_karel", "Bezdak Karel", "Cus Davide. To uz si zpatky z mostu jo? " +
                                                                "Koukej na moji krasnou placatku", "No uz jsem se bal ze ses ztratil.",
                "", "Schovej si to na horsi casy.", null, null, 4);

        KarluvPes karluvPes = new KarluvPes("karluv_pes", "Karluv pes", "", "",
                """
                        Karel: Co delas dopici
                        Ty: Kamo to ne ja to von to sezral sam visco
                        Karel: Davide ty koste ja te videl jak mu to davas *dal ti delo*

                        Vypadl ti predni zub, ale hur uz vypada nemuzes.
                        Karlovi z kapsy vypadla placatka""",
                "Vrrrr!", vajgl, null, 0);

        Chodec chodec = new Chodec("chodec", "Chodec", "Vubec se nepriblizuj bezdaku, cejtim te az sem!",
                "Snad jsem ti neco rekl ne?!", "", "Tahni s tim do hajzlu!", null, null, 17);

        Bezdak bezdak = new Bezdak("spici_bezdak", "Spici bezdak", "Hueh? Co chces? Neses mi cigareku?", "Muh...",
                "Tyjo dik moc Davide. Tady mas 5 korunek.", "Prestan me furt budit.", cigo, null, 0);

        ZatoulanejPes zatoulanejPes = new ZatoulanejPes("zatoulanej_pes", "Zatoulanej pes", "Haf haf!", "Haf!",
                "Vzal si klacek a zbil si s nim zatoulanyho psa. Psi se nebijou. Umrel jsi.", "Haf?", klacik, null, 0);

        Krysa krysa = new Krysa("krysa", "Krysa", "", "", "", "...", null, null, 0);

        Prodavacka prodavacka = new Prodavacka("prodavacka", "Prodavacka",
                """
                        Doby den, dej lahve nebo nakup a vypadni ty bezdak

                        Cigarety: 150 korun
                        Vajnos: 49 korun
                        Mentosky: 15 korun
                        Vodka: 200 korun
                        Vratit lahve: 3 koruny za kus""",
                """
                        Doby den, dej lahve nebo nakup a vypadni ty bezdak
                        Cigarety: 150 korun
                        Vajnos: 49 korun
                        Mentosky: 15 korun
                        Vodka: 200 korun

                        Vratit lahve: 3 koruny za kus""", "Tady mas 3 koruny a tahni! Zakazniky odhanis!", "Odpadky nechci demente.", flaska, null, 0);

        Sekuritak sekuritak = new Sekuritak("sekuritak", "Sekuritak", "", "",
                "Vodpal!", "Jeste jednou a letis!", null, null, 50);

        Revizor revizor = new Revizor("revizor", "Revizor", "Kdybych nebyl strizlivej, tak vas mozna i pustim.",
                "No jo. Po praci honem na pivecko.", "No to je to o cen mluvim! A ted honem utikejte nez si to rozmyslim.",
                "Tohle si radeji nechte pro sebe.", placatka, null, 12);

        Fanousek fanousek = new Fanousek("fanousek", "Fanousek", """
                No hovno to je Posseidon VLT osobne, muzu se s tebou vyfotit?
                Ty: Jasny kamo pojd.
                Fanousek: Jak vubec ted zijes?
                Ty: Dope as fuck. Cigo nemas nahodou?
                Fanousek: Jasny tady mas, a jeste kilovku od boku.
                Ty: Dik kamo.""", "Sorry, zadny ciga ani drobny uz nemam.",
                "", "To si nech Davide, potrebujes to vic jak ja.", null, null, 551);

        Pochybnak pochybnak = new Pochybnak("pochybnak", "pochybnak", "", "",
                "", "Strc si to zpatky do kapsy.", null, null, 0);

        park.vlozPostavu(karluvPes);
        park.vlozPostavu(karel);
        park.vlozPostavu(chodec);
        podMostem.vlozPostavu(bezdak);
        podMostem.vlozPostavu(zatoulanejPes);
        kontejner.vlozPostavu(krysa);
        vecerka.vlozPostavu(prodavacka);
        hlavak.vlozPostavu(sekuritak);
        hlavak.vlozPostavu(revizor);
        metro.vlozPostavu(fanousek);
        naStrzi.vlozPostavu(pochybnak);

        aktualniProstor = park;
    }

    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return aktuální prostor
     */
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        this.notifyObservers();
    }

    public Batoh getBatoh() {
        return batoh;
    }

    public Penezenka getPenezenka() {
        return penezenka;
    }

    @Override
    public void addObserver(IObserver observer) {
        observerSet.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observerSet.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(IObserver observer : observerSet) {
            observer.update();
        }
    }

}
