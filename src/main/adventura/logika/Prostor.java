package logika;

import observer.IObserver;
import observer.ISubject;

import java.util.*;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 * <p>
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class Prostor implements ISubject {

    private final String nazev;
    private final String popis;
    private final String nemuzeVstupit;
    private final Set<Prostor> vychody;   // obsahuje sousední místnosti
    private final Map<String, Vec> veci;
    private final Map<String, Postava> postavy;
    private final double posLeft;
    private final double posTop;
    private static final Set<IObserver> observerSet = new HashSet<>();

    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis, String nemuzeVstupit, double posLeft, double posTop) {
        this.nazev = nazev;
        this.popis = popis;
        this.posLeft = posLeft;
        this.posTop = posTop;
        this.nemuzeVstupit = nemuzeVstupit;
        vychody = new HashSet<>();
        veci = new HashMap<>();
        postavy = new HashMap<>();
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * <p>
     * Bližší popis metody equals je u třídy Object.
     *
     * @param obj object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Prostor druhy)) {
            return false;
        }

        return (java.util.Objects.equals(nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;       
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v mistnosti/prostoru: " + popis + ".\n"
                + popisVychodu() + "\n"
                + popisVeci() + "\n"
                + popisPostav() + "\n";
    }

    /**
     * Vrati text proc hrac nemuze vstoupit do urciteho prostoru
     * @return Text duvodu
     */
    public String getNemuzeVstupit() {
        return nemuzeVstupit;
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        StringBuilder vracenyText = new StringBuilder("Vychody:");
        for (Prostor sousedni : vychody) {
            vracenyText.append(' ').append(sousedni.getNazev());
        }
        return vracenyText.toString();
    }

    /**
     * Vrati popis veci nachazejicich se v prostoru/mistnosti
     * @return Text veci v prostoru
     */
    public String popisVeci() {
        StringBuilder seznamVeci = new StringBuilder("Veci k sebrani: ");
        if (veci.isEmpty()) {
            seznamVeci.append("nic");
        }
        else {
            for (Map.Entry<String, Vec> v : veci.entrySet()) {
                seznamVeci.append(v.getValue().getNazev()).append(", ");
            }
            seznamVeci = new StringBuilder(seznamVeci.substring(0, seznamVeci.length() - 2));
        }
        return seznamVeci.toString();
    }

    /**
     * Vrati seznam postav nachazejicich se v prostoru/mistnosti
     * @return Text postav v prostoru
     */
    public String popisPostav() {
        StringBuilder seznamPostav = new StringBuilder("Postavy: ");
        if (postavy.isEmpty()) {
            seznamPostav.append("zadne");
        }
        else {
            for (Map.Entry<String, Postava> p : postavy.entrySet()) {
                seznamPostav.append(p.getValue().getJmeno()).append(", ");
            }
            seznamPostav = new StringBuilder(seznamPostav.substring(0, seznamPostav.length() - 2));
        }
        return seznamPostav.toString();
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory =
                vychody.stream()
                        .filter(sousedni -> sousedni.getNazev().equals(getNazevProstoru(nazevSouseda))).toList();
        if(hledaneProstory.isEmpty()) {
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Metoda urcena pro hledani prostoru mezi vychody na zaklade
     * uplneho nebo neuplneho nazvu prostoru od uzivatele.
     *
     * @param text Vstupni argument od uzivatele
     * @return Pokud se prostor nasel, vrati jeho cely nazev, pokud ne, vrati se "neobsahuje"
     */
    public String getNazevProstoru(String text) {
        for (Prostor vychod : vychody) {
            if (vychod.getNazev().contains(text)) {
                return vychod.getNazev();
            }
        }
        return "neexistuje";
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    /**
     * Vlozi vec do prostoru/mistnosti
     *
     * @param vec Vec, kterou chceme vlozit
     */
    public void vlozVec(Vec vec) {
        veci.put(vec.getNazev(), vec);
    }

    /**
     * Odebere vec z prostoru/mistnosti
     *
     * @param nazevVeci Nazev veci, kterou chceme odebrat
     * @return Odebrana vec
     */
    public Vec odeberVec(String nazevVeci) {
        return veci.remove(getNazevVeci(nazevVeci));
    }

    /**
     * Vrati, zda se v batohu nachazi pozadovana vec
     *
     * @param nazevVeci Nazev veci, kterou hledame v batohu
     * @return Vrati true, pokud se vec v batohu nachazi, jinak false
     */
    public boolean obsahujeVec(String nazevVeci) {
        return veci.containsKey(getNazevVeci(nazevVeci));
    }

    /**
     * Vrati vec z prostoru/mistnosti
     *
     * @param nazevVeci Nazev veci, kterou chceme getnout
     * @return Pozadovana vec
     */
    public Vec getVec(String nazevVeci) {
        return veci.get(getNazevVeci(nazevVeci));
    }

    /**
     * Metoda urcena pro hledani veci mezi vecmi v prostoru
     * na zaklade uplneho nebo neuplneho nazvu veci od uzivatele.
     *
     * @param text Vstupni argument od uzivatele
     * @return Pokud se vec nasla, vrati jeji cely nazev, pokud ne, vrati se "neobsahuje"
     */
    public String getNazevVeci(String text) {
        for (String nazev : veci.keySet()) {
            if (nazev.contains(text)) {
                return nazev;
            }
        }
        return "neexistuje";
    }

    /**
     * Vlozi postavu do prostoru/mistnosti
     *
     * @param postava Postava, kterou chceme vlozit
     */
    public void vlozPostavu(Postava postava) {
        postavy.put(postava.getJmeno(), postava);
    }

    /**
     * Odebere postavu z prostoru
     *
     * @param jmenoPostavy Jmeno postavy, kterou chceme odebrat
     */
    public Postava odeberPostavu(String jmenoPostavy) {
        return postavy.remove(getJmenoPostavy(jmenoPostavy));
    }

    /**
     * Zjisti, zda prostor obsahuje hledanou postavu
     *
     * @param jmenoPostavy Jmeno postavy
     * @return Vrati true, pokud se postava nachazi v prostoru, jinak false
     */
    public boolean obsahujePostavu(String jmenoPostavy) {
        return postavy.containsKey(getJmenoPostavy(jmenoPostavy));
    }

    /**
     * Vrati postavu z prostoru/mistnosti
     *
     * @param jmenoPostavy Nazev postavy, kterou chceme getnout
     * @return Pozadovana postava
     */
    public Postava getPostava(String jmenoPostavy) {
        return postavy.get(getJmenoPostavy(jmenoPostavy));
    }

    /**
     * Metoda urcena pro hledani postavy mezi postavami v mistnosti
     * na zaklade uplneho nebo neuplneho jmena postavy od uzivatele.
     *
     * @param text Vstupni argument od uzivatele
     * @return Pokud se postava nasela, vrati jeji cele jmeno, pokud ne, vrati se "neexistuje"
     */
    public String getJmenoPostavy(String text) {
        for (String nazev : postavy.keySet()) {
            if (nazev.contains(text)) {
                return nazev;
            }
        }
        return "neexistuje";
    }
    public double getPosLeft() {
        return posLeft;
    }

    public double getPosTop() {
        return posTop;
    }

    public Set<String> getMnozinaNazvuVeciVProstoru() {
        return veci.keySet();
    }

    @Override
    public void addObserver(IObserver observer) {
        observerSet.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observerSet.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : observerSet) {
            observer.update();
        }
    }

}
