package gui;

import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import logika.Hra;
import logika.prikazy.PrikazPolozit;
import observer.IObserver;

public class PanelBatohu implements IObserver {

    private final FlowPane flowPane = new FlowPane();
    private final VBox vBox = new VBox();
    private final TextArea textArea;

    public PanelBatohu(TextArea textArea) {
        this.textArea = textArea;
        Hra.getHra().getHerniPlan().getBatoh().addObserver(this);

        vBox.getChildren().addAll(new Label("Věci v batohu: "), flowPane);
        vBox.setMaxWidth(100.0);

        update();
    }

    public VBox getVBox() {
        return this.vBox;
    }

    @Override
    public void update() {
        flowPane.getChildren().clear();
        for (String nazevVeci : Hra.getHra().getHerniPlan().getBatoh().getMnozinaNazvuVeciVBatohu()) {
            Image image = new Image(PanelVeci.class.getResourceAsStream("/items/" + nazevVeci + ".png"), 100,100, false,false);
            ImageView imageView = new ImageView(image);
            imageView.setOnMouseClicked(mouseEvent -> textArea.appendText("\n" +
                    Hra.getHra().zpracujPrikaz(PrikazPolozit.NAZEV + " " + nazevVeci) + "\n"));
            flowPane.getChildren().addAll(imageView);
        }
    }

}
