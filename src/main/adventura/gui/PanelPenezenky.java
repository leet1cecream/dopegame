package gui;

import javafx.scene.control.Label;
import logika.Hra;
import observer.IObserver;

public class PanelPenezenky implements IObserver {

    private final Label label = new Label();

    public PanelPenezenky() {
        Hra.getHra().getHerniPlan().getPenezenka().addObserver(this);
        label.setText("Peněženka: " + Hra.getHra().getHerniPlan().getPenezenka().getPenize());
        update();
    }

    public Label getLabel() {
        return this.label;
    }

    @Override
    public void update() {
        label.setText("Peněženka: " + Hra.getHra().getHerniPlan().getPenezenka().getPenize());
    }

}
