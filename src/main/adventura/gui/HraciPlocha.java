package gui;

import logika.Hra;
import logika.Prostor;
import observer.IObserver;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class HraciPlocha implements IObserver {
    private final AnchorPane anchorPane;
    private final ImageView obrazekHrace;

    public HraciPlocha() {
        Hra.getHra().getHerniPlan().addObserver(this);
        anchorPane = new AnchorPane();
        obrazekHrace = new ImageView(new Image(getClass().getResourceAsStream("/david.png"),
                80.0, 0, true, false));
        ImageView imageViewMapy = new ImageView(new Image(getClass().getResourceAsStream("/mapa.png"),
                1000.0, 0, true, false));
        anchorPane.getChildren().addAll(imageViewMapy, obrazekHrace);

        update();
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    @Override
    public void update() {
        Prostor aktualniProstor = Hra.getHra().getHerniPlan().getAktualniProstor();
        AnchorPane.setLeftAnchor(obrazekHrace, aktualniProstor.getPosLeft());
        AnchorPane.setTopAnchor(obrazekHrace, aktualniProstor.getPosTop());
    }
}
