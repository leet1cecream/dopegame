package gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import logika.Hra;
import logika.Prostor;
import observer.IObserver;

import java.util.Collection;

public class PanelVychodu implements IObserver {
    private final ObservableList<String> observableList = FXCollections.observableArrayList();
    private final ListView<String> listView = new ListView<>(observableList);

    public PanelVychodu() {
        Hra.getHra().getHerniPlan().addObserver(this);
        listView.setMaxWidth(150);

        update();
    }

    private void nastavVychody() {
        observableList.clear();
        Collection<Prostor> vychody = Hra.getHra().getHerniPlan().getAktualniProstor().getVychody();
        for (Prostor vychod : vychody) {
            observableList.add(vychod.getNazev());
        }
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        nastavVychody();
    }
}
