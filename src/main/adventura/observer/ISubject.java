package observer;

/**
 * Interface subject, ktery je soucasti navrhoveho vzoru observer.
 */
public interface ISubject {

    /**
     * Metod a vlozi poskytnuty observer do member listu tridy,
     * ktera tuto metodu implementuje.
     *
     * @param observer Observer, ktery chce dostavat updaty
     */
    void addObserver(IObserver observer);

    /**
     * Metoda odebere poskytnuty observer z member listu tridy,
     * ktera tuto metodu implementuje
     *
     * @param observer Observer, ktery se chce odebrat
     */
    void removeObserver(IObserver observer);

    /**
     * Metoda upozorni vsechni observery na zmenu stavu pozorovane,
     * tridy.
     */
    void notifyObservers();

}
