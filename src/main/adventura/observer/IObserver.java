package observer;

/**
 * Interface observer, krery je soucasti nabrhveho vzoru observer.
 */
public interface IObserver {

    /**
     * Metoda dle implementace tridou jedna na zaklade zmeny stavu
     * sledovane tridy.
     */
    void update();

}
