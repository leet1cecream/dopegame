package logika;

import logika.prikazy.PrikazJit;
import logika.prikazy.PrikazMluvit;
import logika.prikazy.PrikazSebrat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HraTest {

    Hra hra;

    @BeforeEach
    void setup() {
        hra = Hra.resetHra();
    }

    @Test
    @DisplayName("Zpracovani prikazu")
    void zpracujPrikaz() {
        hra.zpracujPrikaz(PrikazMluvit.NAZEV + " chodec");
        assertTrue(hra.getHerniPlan().getAktualniProstor().getPostava("chodec").getMluvil());

        hra.zpracujPrikaz(PrikazSebrat.NAZEV + " vajgl");
        assertTrue(hra.getHerniPlan().getBatoh().obsahujeVec("vajgl"));

        hra.zpracujPrikaz(PrikazJit.NAZEV + " pod_mostem");
        assertEquals(hra.getHerniPlan().getAktualniProstor().getNazev(), "pod_mostem");
    }

    @Test
    @DisplayName("Kompletni restart hry")
    void resetHra() {
        zpracujPrikaz();
        hra = Hra.resetHra();

        assertNotEquals(hra.getHerniPlan().getAktualniProstor().getNazev(), "pod_mostem");
        assertFalse(hra.getHerniPlan().getBatoh().obsahujeVec("vajgl"));
        assertFalse(hra.getHerniPlan().getAktualniProstor().getPostava("chodec").getMluvil());
    }

    @Test
    @DisplayName("Konec hry pri chybe")
    void konecHry() {
        hra.zpracujPrikaz(PrikazMluvit.NAZEV + " karluv_pes");
        assertTrue(hra.konecHry());
    }

}