package logika;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProstorTest {

    Prostor prostor1;
    Prostor prostor2;
    Vec vec1;
    Vec vec2;
    Postava postava1;
    Postava postava2;

    @BeforeEach
    void setup() {
        prostor1 = new Prostor("prostor1", "prostor1", "", 0, 0);
        prostor2 = new Prostor("prostor2", "prostor2", "", 0, 0);
        vec1 = new Vec("vec1", "vec1", false, 0);
        vec2 = new Vec("vec2", "vec2", false, 0);
        postava1 = new Postava("postava1", "postava1", "pred", "po", "chce", "nechce", null, null, 0);
        postava2 = new Postava("postava2", "postava2", "pred", "po", "chce", "nechce", null, null, 0);
    }

    @Test
    @DisplayName("Vlozeni vychodu do jineho prostoru")
    void setVychod() {
        prostor1.setVychod(prostor2);
        prostor2.setVychod(prostor1);
        assertEquals(prostor1.vratSousedniProstor(prostor2.getNazev()), prostor2);
        assertEquals(prostor2.vratSousedniProstor(prostor1.getNazev()), prostor1);
    }

    @Test
    @DisplayName("Ziskani vychodu pomoci jmena vychodu")
    void vratSousedniProstor() {
        setVychod();

        assertEquals(prostor1.vratSousedniProstor(prostor2.getNazev()), prostor2);
        assertNull(prostor1.vratSousedniProstor("neexistuje"));
    }

    @Test
    @DisplayName("Vraceni kolekce vychodu")
    void getVychody() {
        setVychod();

        assertTrue(prostor1.getVychody().contains(prostor2));
        assertTrue(prostor2.getVychody().contains(prostor1));
        assertFalse(prostor1.getVychody().contains(prostor1));
        assertFalse(prostor2.getVychody().contains(prostor2));
    }

    @Test
    @DisplayName("Vlozeni nekolika vec")
    void vlozVec() {
        prostor1.vlozVec(vec1);
        prostor1.vlozVec(vec2);

        assertEquals(prostor1.getVec(vec1.getNazev()), vec1);
        assertEquals(prostor1.getVec(vec2.getNazev()), vec2);
        assertNull(prostor1.getVec("neobsahuje"));
    }

    @Test
    @DisplayName("Odebrani veci")
    void odeberVec() {
        vlozVec();

        assertEquals(prostor1.odeberVec(vec1.getNazev()), vec1);
        assertNull(prostor1.odeberVec(vec1.getNazev()));
        assertFalse(prostor1.obsahujeVec(vec1.getNazev()));
    }

    @Test
    @DisplayName("Obsahuje vec")
    void obsahujeVec() {
        vlozVec();

        assertTrue(prostor1.obsahujeVec(vec1.getNazev()));
        assertFalse(prostor1.obsahujeVec("neobsahuje"));
        assertFalse(prostor2.obsahujeVec("neobsahuje"));
    }

    @Test
    @DisplayName("Vlozeni postav")
    void vlozPostavu() {
        prostor1.vlozPostavu(postava1);
        prostor1.vlozPostavu(postava2);

        assertEquals(prostor1.getPostava(postava1.getJmeno()), postava1);
        assertEquals(prostor1.getPostava(postava2.getJmeno()), postava2);
        assertNull(prostor1.getPostava("neobsahuje"));
    }

    @Test
    @DisplayName("Odebrani postav")
    void odeberPostavu() {
        vlozPostavu();

        assertEquals(prostor1.odeberPostavu(postava1.getJmeno()), postava1);
        assertNull(prostor1.odeberPostavu(postava1.getJmeno()));
        System.out.println(prostor1.popisPostav());
        assertFalse(prostor1.obsahujePostavu(postava1.getJmeno()));
    }

    @Test
    @DisplayName("Obsahuje postavy")
    void obsahujePostavu() {
        vlozPostavu();

        assertTrue(prostor1.obsahujePostavu(postava1.getJmeno()));
        assertFalse(prostor1.obsahujePostavu("neobsahuje"));
        assertFalse(prostor2.obsahujePostavu("neobsahuje"));
    }
}