package logika;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PostavaTest {

    Postava postava;
    Vec vecChce;
    Vec vecMa;

    @BeforeEach
    void setup() {
        vecChce = new Vec("vecChce", "vecChce", false, 0);
        vecMa = new Vec("vecMa", "vecMa", false, 0);
        postava = new Postava("jmeno", "jmeno", "pred", "po", "chce", "nechce", vecChce, vecMa, 10);
    }

    @Test
    @DisplayName("Mluveni s postavou pred a po prvni interakci")
    void mluvit() {
        assertEquals(postava.getScreenJmeno() + ": " + postava.getMluvPred(), postava.mluvit().trim());
        assertNotEquals(postava.getScreenJmeno() + ": " + postava.getMluvPred(), postava.mluvit().trim());
        assertEquals(postava.getScreenJmeno() + ": " + postava.getMluvPo(), postava.mluvit().trim());
    }

    @Test
    @DisplayName("Vymena dvou veci")
    void vymenitVec() {
        assertFalse(postava.getVymena());
        assertEquals(vecMa, postava.vymenitVec(vecChce));
        assertTrue(postava.getVymena());
    }
}