package logika;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PenezenkaTest {

    Penezenka penezenka;

    @BeforeEach
    void setup() {
        penezenka = new Penezenka();
    }

    @Test
    @DisplayName("Pridani urcite sumy penez")
    void pridatPenize() {
        int before = penezenka.getPenize();
        penezenka.pridatPenize(100);
        assertEquals(penezenka.getPenize(), before + 100);
    }

    @Test
    @DisplayName("Odebrani (presahujici) sumy")
    void odebratPenize() {
        pridatPenize();

        assertTrue(penezenka.odebratPenize(penezenka.getPenize()));
        assertFalse(penezenka.odebratPenize(1));
    }
}