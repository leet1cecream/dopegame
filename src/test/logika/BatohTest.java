package logika;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BatohTest {

    Batoh batoh;
    ArrayList<Vec> seznamVeci;

    @BeforeEach
    void setup() {
        batoh = new Batoh();
        seznamVeci = new ArrayList<>();
        for(int i = 0; i <= Batoh.VELIKOST; ++i) {
            seznamVeci.add(new Vec(Integer.toString(i), Integer.toString(i), false, 0));
        }
    }

    @Test
    @DisplayName("Vlozeni nekolika veci")
    void vlozVec() {
        for(int i = 0; i < Batoh.VELIKOST; ++i) {
            assertTrue(batoh.vlozVec(seznamVeci.get(i)));
        }
    }

    @Test
    @DisplayName("Vyndani existujici i neexistujici veci")
    void vyndejVec() {
        vlozVec();

        assertNull(batoh.vyndejVec("neexistuje"));

        for(int i = 0; i < Batoh.VELIKOST; ++i) {
            assertEquals(seznamVeci.get(i), batoh.vyndejVec(Integer.toString(i)));
        }

        assertNull(batoh.vyndejVec("0"));
    }

    @Test
    @DisplayName("Obsahuje nebo neobsahuje vec")
    void obsahujeVec() {
        vlozVec();

        for(int i = 0; i < Batoh.VELIKOST; ++i) {
            assertTrue(batoh.obsahujeVec(Integer.toString(i)));
        }


        assertFalse(batoh.obsahujeVec("neobsahuje"));
    }

    @Test
    @DisplayName("Vlozeni veci pres maximalni velikost")
    void jePlny() {
        vlozVec();

        assertFalse(batoh.vlozVec(seznamVeci.get(Batoh.VELIKOST)));
        assertFalse(batoh.obsahujeVec(Integer.toString(Batoh.VELIKOST)), "");
    }
}