# Dope game

## Obsah
* [Obecne informace](#obecne-informace)
* [Popis hry](#popis-hry)
* [Prikazy](#prikazy)
* [Spusteni](#spusteni)

## Obecne informace
Tento projekt je vysledkem seminarnich praci predmetu 4IT101 (kod hry) a 4IT115 (kod GUI).

## Popis hry
Pise se rok 2022 a zijes uz dlouhych 5 let na ulici.
Tvoje jmeno je David Stilip a jedine co si kazdy den prejes je davka perniku.
Sehnat piko bez penez ale bohuzel neni jednoduche a proto sis uz fakt dlouho nestrelil.

Cilem hry je sjet se jak pes a zustat real.
Tento ukol vsak nebude jednoduchy, protoze se s bezdakama jen tak nikdo nebavi
a navic mas na sobe poblity vintage versace jeans.

## Prikazy
* `jit [prostor]` - Prechazi mezi prostory
* `sebrat [vec]` - Sebere vec z prostoru
* `polozit [vec]` - Polozi vec do prostoru
* `dat [vec] [predmet]` - Da predmet postave
* `penezenka` - Zobrazi obsah penezenky
* `batoh` - Zobrazi obsah batohu
* `mluvit [postava]` - Mluvi s postavou
* `ukrast [vec]` - Ukradne vec
* `okrast [postava]` - Okradne postavu
* `fetovat` - Vyhraje hru
* `napoveda` - Zobrazi napovedu
* `konec` - Ukonci hru

## Spusteni
Pro spusteni je potreba zkompilovat kod do .jar souboru. Pote v prislusnem adresari 
do terminalu zadejte prikaz ```java -jar x.jar```, kde "x" je nazev aplikace.
